#pragma once

#ifndef STUDENT_H
#define STUDENT_H
#include <string>
using namespace std;



class Student
{
public:
	void setStudent(string _name, int _id);
	void print();
	int calculateGrade();

private:
	string name;
	int id;
	int midTermExam;
	int finalExam;
};

#endif 
